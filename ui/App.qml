import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.2
import QtQuick.Extras 1.4
import QtQuick.Layouts 1.3
import QtQuick.Window 2.2


ApplicationWindow {
    property int defMargin: 10
    property int switchModeOffset: 100

    signal update_display(string symbol)
    signal solve()

    id: appWindow
    visible: true
    width: 300
    minimumWidth: 300
    height: 400
    minimumHeight: 400
    maximumHeight: 400


    title: (appWindow.width > appWindow.minimumWidth + switchModeOffset)  ? qsTr("Calculator [Extra]") : qsTr("Calculator [Normal]")


    Calculator {}

}