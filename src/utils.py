
FUNC_EXPRESSIONS = {
    # MATH FUNCTIONS
    'sqrt(..)':'sqrt({0})',
    'sin(..)':'sin({0})',
    'cos(..)':'cos({0})',
    'tan(..)':'tan({0})',
    'tanh(..)':'tanh({0})',
    'log(..)':'log({0}, ',

    # OTHER
    '(..)':'({0})',
    'AC':'',
}