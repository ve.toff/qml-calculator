from PySide2 import QtGui
from src.ui import UICalculator


class App(QtGui.QGuiApplication):
    def __init__(self, args):
        super(App, self).__init__(args)

        self.ui_calculator = UICalculator()
