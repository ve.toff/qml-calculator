import os

BASE_DIR = os.path.dirname(os.path.abspath(__name__))

# Application info
APPLICATION_VERSION = '0.0.2'
