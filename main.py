import sys
from src.app import App


if __name__ == '__main__':
    # sys.argv += ['--style', 'material']
    app = App(sys.argv)
    sys.exit(app.exec_())
